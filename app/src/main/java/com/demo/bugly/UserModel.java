package com.demo.bugly;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.multibindings.IntoSet;

/**
 * Created by zzt
 * 2021/1/4
 */

@Module
@InstallIn(ActivityComponent.class)
public class UserModel {
    @IntoSet
    @Provides
    public String provideString(){
        return "user";
    }

}
