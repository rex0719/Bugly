package com.demo.bugly;

import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Nullable;

import javax.inject.Inject;

/**
 * Created by zzt
 * 2021/1/4
 */
public class Fragment2 extends BaseFragment {
    @Inject
    public Fragment2() {
    }


    @Override
    public int bindLayout() {
        return R.layout.fragment2;
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        Log.d(">>>>>>>>>>>>","fragment2");
    }

    @Override
    public boolean useEventBus() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(">>>>>>>>>>>>","fragment2--onResume");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(">>>>>>","fragment2--onDestroy");
    }
}
