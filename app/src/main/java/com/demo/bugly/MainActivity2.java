package com.demo.bugly;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.demo.bugly.Dialog.InputDialogUtils;

public class MainActivity2 extends AppCompatActivity {

    private InputDialogUtils inputDialogUtils;
    Button btn_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main2);
        btn_btn = findViewById(R.id.btn_btn);
        String format = String.format(getResources().getString(R.string.btn_name), "梁志超他奶");
        btn_btn.setText(format);

        btn_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (inputDialogUtils == null) {
                    inputDialogUtils = new InputDialogUtils(MainActivity2.this, R.style.BottomDialog2);
                } else {
                    inputDialogUtils.show();
                }
                inputDialogUtils.setListener(new InputDialogUtils.InputDialogListener() {
                    @Override
                    public void onConfirm(int type) {

                    }

                    @Override
                    public void onCancel(EditText editText) {
                        inputDialogUtils.cancel();
                    }

                    @Override
                    public void getInputTxt(EditText editText) {
                        Toast.makeText(MainActivity2.this, editText.getText().toString(), Toast.LENGTH_SHORT).show();
                        inputDialogUtils.dismiss();
                    }
                });
            }
        });

//        View decorView = getWindow().getDecorView();
//        View contentView = findViewById(Window.ID_ANDROID_CONTENT);
//        decorView.getViewTreeObserver().addOnGlobalLayoutListener(getGlobalLayoutListener(decorView, contentView));


    }

    private ViewTreeObserver.OnGlobalLayoutListener getGlobalLayoutListener(final View decorView, final View contentView) {
        return new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                decorView.getWindowVisibleDisplayFrame(r);

                int height = decorView.getContext().getResources().getDisplayMetrics().heightPixels;
                int diff = height - r.bottom;

                if (diff != 0) {
                    if (contentView.getPaddingBottom() != diff) {
                        contentView.setPadding(0, 0, 0, diff);
                    }
                } else {
                    if (contentView.getPaddingBottom() != 0) {
                        contentView.setPadding(0, 0, 0, 0);
                    }
                }
            }
        };
    }
}