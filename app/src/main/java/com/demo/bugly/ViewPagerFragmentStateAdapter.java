package com.demo.bugly;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.List;

/**
 * Created by zzt
 * 2021/1/4
 */
public class ViewPagerFragmentStateAdapter extends FragmentStateAdapter {
    List<Fragment> mList;

    public ViewPagerFragmentStateAdapter(@NonNull FragmentActivity fragmentActivity, List<Fragment> mList) {
        super(fragmentActivity);
        this.mList = mList;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
