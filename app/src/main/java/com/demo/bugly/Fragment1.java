package com.demo.bugly;

import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Nullable;

import javax.inject.Inject;

/**
 * Created by zzt
 * 2021/1/4
 */
public class Fragment1 extends BaseFragment {
    @Inject
    public Fragment1() {
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment1;
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        Log.d(">>>>>>>>>>>>","fragment1");
    }

    @Override
    public boolean useEventBus() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(">>>>>>>>>>>>","fragment1--onResume");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(">>>>>>","fragment1--onDestroy");
    }
}
