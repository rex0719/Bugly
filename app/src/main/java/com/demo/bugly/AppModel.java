package com.demo.bugly;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import dagger.multibindings.IntoSet;

/**
 * Created by zzt
 * 2021/1/4
 */
@Module
@InstallIn(ApplicationComponent.class)
public class AppModel {
    @IntoSet
    @Provides
    public String provideString() {
        return "app";
    }
}
