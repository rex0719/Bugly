package com.demo.bugly;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;

/**
 * Created by zzt
 * 2021/1/4
 */


public  class User {
    public String id;
    public String name;
    public String from;
    @Inject
    public User(){
        this("1","lisi", "购物车");
    }

    public User(String id, String name, String from) {
        this.id = id;
        this.name = name;
        this.from = from;
    }



}

