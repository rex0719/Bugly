package com.demo.bugly;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.util.Log;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.tencent.bugly.crashreport.common.info.PlugInBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

import static androidx.viewpager2.widget.ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT;

@AndroidEntryPoint
public class NewActivity extends AppCompatActivity {
    @Inject
    public Set<String> set;
    @Inject
    Fragment1 fragment1;
    @Inject
    Fragment2 fragment2;
    @Inject
    Fragment3 fragment3;
    @Inject
    Fragment4 fragment4;
    @Inject
    Fragment5 fragment5;
    @Inject
    Fragment6 fragment6;


    ViewPager2 vp2;
    TabLayout mTabLayout;
    private List<Fragment> mList = new ArrayList<>();
    private ViewPagerFragmentStateAdapter mAdapter;

    public static final int OFFSCREEN = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        vp2 = findViewById(R.id.viewpager2);
        mTabLayout = findViewById(R.id.tablayout);

        mList.add(fragment1);
        mList.add(fragment2);
        mList.add(fragment3);
        mList.add(fragment4);
        mList.add(fragment5);
        mList.add(fragment6);






        mAdapter = new ViewPagerFragmentStateAdapter(this, mList);
        vp2.setAdapter(mAdapter);
        vp2.setUserInputEnabled(false);
        vp2.setOffscreenPageLimit(OFFSCREEN_PAGE_LIMIT_DEFAULT);

        // 添加页签选中监听
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        // 注册页面变化的回调接口
        vp2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                mTabLayout.setScrollPosition(position, 0, false);
            }
        });

        new TabLayoutMediator(mTabLayout,vp2,true, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(position+"");
            }
        }).attach();
    }


}