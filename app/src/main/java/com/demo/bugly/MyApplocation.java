package com.demo.bugly;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.tencent.bugly.crashreport.CrashReport;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import dagger.hilt.android.HiltAndroidApp;

/**
 * Created by zzt
 * 2020/12/30
 */

//Hilt程序的入口 使用Hilt必须以Application当作入口
@HiltAndroidApp
public class MyApplocation extends Application {
    /*** 项目全局上下文 */
    private static MyApplocation mAppContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppContext = this;
        Context context = getApplicationContext();
        // 获取当前包名
        String packageName = context.getPackageName();
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());
        // 设置是否为上报进程
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(context);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));
        // 初始化Bugly
        CrashReport.initCrashReport(getApplicationContext(), "c5b52efbaa", true, strategy);

        DeviceAdminUtil.getInstance().init(mAppContext);


//        CardUtils.init();

    }

    public static MyApplocation getAppContext() {
        return mAppContext;
    }

    private static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }
}
