package com.demo.bugly.view;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MyTextView extends androidx.appcompat.widget.AppCompatTextView {

    private Paint txtPaint;
    private Paint linePaint;
    //控件高度
    private int height;
    //控件宽度
    private int width;

    public MyTextView(Context context) {
        super(context);
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        //创建文字画笔对象

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureHeight(int measureHeight) {
        int specMode = MeasureSpec.getMode(measureHeight);
        int specSize = MeasureSpec.getSize(measureHeight);

        int defaultHeight = 0;

        switch (specMode) {
            case MeasureSpec.AT_MOST:
                /*
                相当于设置为wrap_content
                等于100的话相当于父布局设置为wrap_content时会给这个子view留有100px的空间
                值要设置为wrap_content就会有100px空，不管父布局怎么设置 都不会受到影响
                当给子控件设置为固定值时 不管设置为多少时，依然会是100px的大小
                */
                defaultHeight = 100;
                Log.d(">>>>height","AT_MOST");
                break;
            case MeasureSpec.EXACTLY:
                /*
                相当于设置为match_content或是一个具体的值
                返回300的话，当此控件设置为match_content时最大为300px
                如果此控件设置为match_content父布局设置为wrap_content的话，依旧会保留300px
                */
                defaultHeight = 300;
                Log.d(">>>>height","EXACTLY");
                break;
            case MeasureSpec.UNSPECIFIED://父布局没有给子view任何的限制 表示默认值 我也不知道这个什么时候会调用
                defaultHeight = specSize;
                Log.d(">>>>height","UNSPECIFIED");
                break;
        }
        return defaultHeight;
    }

    private int measureWidth(int measureWidth) {
        int specMode = MeasureSpec.getMode(measureWidth);
        int specSize = MeasureSpec.getSize(measureWidth);

        int defaultWidth = 0;

        switch (specMode) {
            case MeasureSpec.AT_MOST://相当于设置为wrap_content
                defaultWidth = 100;
                Log.d(">>>>Width","AT_MOST");
                break;
            case MeasureSpec.EXACTLY://相当于设置为match_content或是一个具体的值
                defaultWidth = 300;
                Log.d(">>>>Width","EXACTLY");
                break;
            case MeasureSpec.UNSPECIFIED://父布局没有给子view任何的限制 表示默认值
                defaultWidth = specSize;
                Log.d(">>>>Width","UNSPECIFIED");
                break;
        }
        return defaultWidth;
    }

}
