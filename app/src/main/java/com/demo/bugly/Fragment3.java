package com.demo.bugly;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import javax.inject.Inject;

/**
 * Created by zzt
 * 2021/1/4
 */
public class Fragment3 extends BaseFragment {
    @Inject
    public Fragment3() {
    }


    @Override
    public int bindLayout() {
        return R.layout.fragment3;
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        Log.d(">>>>>>>>>>>>","fragment3");
    }

    @Override
    public boolean useEventBus() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(">>>>>>>>>>>>","fragment3--onResume");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(">>>>>>","fragment3--onDestroy");
    }
}
