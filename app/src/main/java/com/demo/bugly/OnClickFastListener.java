package com.demo.bugly;

import android.view.View;

/**
 * Created by zzt
 * 2021/1/8
 */
public abstract class OnClickFastListener implements View.OnClickListener {
    private static long lastClickTime;
    private long DELAY_TIME = 1000;

    public abstract void onFastClick(View view);

    private boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < this.DELAY_TIME) {
            return true;
        }
        lastClickTime = time;
        return false;
    }
    @Override
    public void onClick(View view) {
        if (!isFastDoubleClick()) {
            onFastClick(view);
        }
    }

    public OnClickFastListener setLastClickTime(long delay_time) {
        this.DELAY_TIME = delay_time;
        return this;
    }
}
