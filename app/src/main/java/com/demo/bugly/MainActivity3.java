package com.demo.bugly;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.demo.bugly.view.MyTextView;

import java.lang.ref.WeakReference;

public class MainActivity3 extends AppCompatActivity {

    MyTextView mytextview;
    private MyHandler myHandler=new MyHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        mytextview=findViewById(R.id.mytextview);

        mytextview.setOnClickListener(new OnClickFastListener() {
            @Override
            public void onFastClick(View view) {
//                Log.d("QQQ","111");
//                DeviceAdminUtil.getInstance().lockScreen();
                Message obtain = Message.obtain();
                obtain.what = 100;
                obtain.obj="aaaaa";
                myHandler.sendMessage(obtain);
            }
        });





    }



    private static class MyHandler extends Handler{
        private WeakReference<MainActivity3> weakReference;

        public MyHandler(MainActivity3 mainActivity3) {
            weakReference = new WeakReference<>(mainActivity3);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if(msg.what==100){
                Log.d("zzts", (String) msg.obj);
            }
        }
    }

}