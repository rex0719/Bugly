package com.demo.bugly.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.demo.bugly.R;

/**
 * Created by zzt
 * 2021/1/5
 */
public class InputDialogUtils extends Dialog {
    private Context mContext;//上下文对象

    private EditText editText;//输入框对象

    private InputDialogListener listener;//监听回调

    public void setListener(InputDialogListener listener) {
        this.listener = listener;
    }

    public InputDialogUtils(@NonNull Context context) {
        super(context);
        this.mContext = context;
        initDialog();
    }

    public InputDialogUtils(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.mContext = context;
        initDialog();
    }

    protected InputDialogUtils(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.mContext = context;
        initDialog();
    }

    private void initDialog() {
        View view = LinearLayout.inflate(mContext, R.layout.pop_view_session_input, null);
        setContentView(view);

        //输入框
        editText = view.findViewById(R.id.ed_message);

        Window window = getWindow();
        window.setGravity(Gravity.BOTTOM);
        //这里的动画效果自己创建
        window.setWindowAnimations(R.style.BottomDialog_Animation);

        //添加发送点击事件
        view.findViewById(R.id.tv_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.getInputTxt(editText);
                    editText.setText("");
                }
            }
        });

        //添加放大点击事件
        view.findViewById(R.id.tv_big).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        setCancelable(true);
        show();
        //保证自定义得布局文件充满屏幕宽度
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = (int) (ScreenUtils.getScreenWidth(mContext)); //设置宽度
        getWindow().setAttributes(lp);
    }

    @Override
    public void dismiss() {
        KeyboardUtils.hideSoftInput(editText);
        //与键盘解绑
        editText.setFocusable(false);
        editText.setShowSoftInputOnFocus(false);
        editText.setFocusableInTouchMode(false);
        editText.requestFocus();
        super.dismiss();
    }

    @Override
    public void show() {
        //显示键盘
        KeyboardUtils.showSoftInput();
        //与键盘绑定
        editText.setFocusable(true);
        editText.setShowSoftInputOnFocus(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        super.show();
    }


   public interface  InputDialogListener{
        void getInputTxt(EditText editText);
        void onCancel(EditText editText);
        void onConfirm(int type);
    }
}
