package com.demo.bugly;

import android.os.Bundle;

import androidx.annotation.Nullable;

public interface IFragment {
    //绑定布局
    int bindLayout();

    /**
     * 初始化数据
     *
     * @param savedInstanceState
     */
    void initData(@Nullable Bundle savedInstanceState);

    /**
     * 是否使用 EventBus
     *
     * @return
     */
    boolean useEventBus();
}
