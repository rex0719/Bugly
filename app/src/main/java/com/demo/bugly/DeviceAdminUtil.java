package com.demo.bugly;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.util.Log;

import com.demo.bugly.view.MyAdminReceiver;

import java.lang.reflect.Method;

/**
 * Created by zzt
 * 2021/4/26
 */
public class DeviceAdminUtil {

    private static DeviceAdminUtil instance;

    private DevicePolicyManager devicePolicyManager;

    private ComponentName componentName;

    private Context mContext;


    public static synchronized DeviceAdminUtil getInstance() {
        if (instance == null) {
            instance = new DeviceAdminUtil();
        }
        return instance;
    }

    /**
     *     * 初始化“设备管理权限的获取”
     *     *
     *     * @param context
     *
     */

    public void init(Context context) {
        mContext = context.getApplicationContext();
        // 获取系统管理权限

        devicePolicyManager = (DevicePolicyManager) mContext.getSystemService(Context.DEVICE_POLICY_SERVICE);
        // 申请权限
        componentName = new ComponentName(mContext, MyAdminReceiver.class);
        setDeviceAdminActive(true);
    }

    /**
     *     * 判断App是否已激活获取了设备管理权限,true:已激活，false:未激活
     *     *
     *     * @return
     *
     */

    private boolean isAdminActive() {
// 判断组件是否有系统管理员权限
        return devicePolicyManager.isAdminActive(componentName);
    }

    public void lockScreen() {
        if (isAdminActive() && devicePolicyManager != null) {
            // 立刻锁屏
            devicePolicyManager.lockNow();
        }
    }

    private void setDeviceAdminActive(boolean active) {
      try {
        if (devicePolicyManager != null && componentName != null) {
          Method setActiveAdmin = devicePolicyManager.getClass().getDeclaredMethod("setActiveAdmin", ComponentName.class, boolean.class);
          setActiveAdmin.setAccessible(true);
          setActiveAdmin.invoke(devicePolicyManager, componentName, active);
        }
      } catch (Exception e) {

      }
    }


}
